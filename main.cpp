#include <iostream>

using namespace std;

struct Node {
    int data;
    Node *next;
};

Node *make_node(Node *next, int data);
void add_node(Node *start, int data);
void print_list(Node *start, bool vertical);
Node *delete_node(Node *start, int target);
Node *find_last_node(Node *start);
void delete_list(Node *start);
Node *reverse_list(Node *start);
void insert_node(Node *start, int data, int target);
void merge_lists(Node *start1, Node *start2);
Node *move_start(Node *start, int target);

void play_josephus();

void test_list();

int main() {
    cout << "Testing list..." << endl;
    test_list();
    cout << endl << "Play Josephus:" << endl;
    play_josephus();
    return 0;
}

void test_list() {
    cout << "Make list1=[1, 2, 5, 23]" << endl;
    Node *start = make_node(nullptr, 1);
    add_node(start, 2);
    add_node(start, 5);
    add_node(start, 23);
    print_list(start, false);

    cout << "Delete node=5 & Insert node=3 after node=2" << endl;
    start = delete_node(start, 5);
    insert_node(start, 3, 2);
    print_list(start, false);

    cout << "Reverse list1" << endl;
    start = reverse_list(start);
    print_list(start, false);

    cout << "Merge list1 with list2=[100, 200, 500]" << endl;
    Node *start2 = make_node(nullptr, 100);
    add_node(start2, 200);
    add_node(start2, 500);
    merge_lists(start, start2);
    print_list(start, false);

    cout << "Move list1 start to node=100" << endl;
    start = move_start(start, 100);
    print_list(start, false);

    cout << "Test Finished." << endl;
}

/*
 * Online Josephus:
 * https://www.geogebra.org/m/ExvvrBbR
 * */
void play_josephus() {
    int seats;

    cout << "Enter number of soldiers (>= 2): ";
    cin >> seats;
    if (seats < 2) {
        cout << "You stay alive!";
        return;
    }

    Node *start = make_node(nullptr, 1);
    for (int i = 2; i <= seats; i++) {
        add_node(start, i);
    }

    Node *killer = start;
    while (killer->next != killer) {
        Node *to_be_killed = killer->next;
        Node *next_killer = killer->next->next;
        killer->next = next_killer;
        delete to_be_killed;
        killer = next_killer;
    }
    cout << killer->data << " is alive at the end!";
}

Node *make_node(Node *next, int data) {
    Node *node = new(Node);
    node->data = data;
    if (next == nullptr) {
        node->next = node;
    } else {
        node->next = next;
    }
    return node;
}

void add_node(Node *start, int data) {
    Node *last_node = find_last_node(start);
    Node *new_node = make_node(start, data);
    last_node->next = new_node;
}

Node *find_last_node(Node *start) {
    Node *last_node = start;
    while (last_node->next != start) {
        last_node = last_node->next;
    }
    return last_node;
}

void print_list(Node *start, bool vertical) {
    if (start == nullptr) {
        cout << "Start is null!";
        return;
    }

    Node *node = start;
    do {
        cout << node->data;
        if (vertical) {
            cout << endl;
        } else {
            cout << "\t";
        }
        node = node->next;
    } while (node != start);

    cout << endl;
}

Node *delete_node(Node *start, int target) {
    if (start->data == target) {
        if (start->next == start) {
            delete start;
            return nullptr;
        }

        Node *last_node = find_last_node(start);
        last_node->next = start->next;

        delete start;
        return last_node->next;
    }

    Node *previous_node = start;
    Node *node = start->next;
    do {
        if (node->data == target) {
            previous_node->next = node->next;
            delete node;
            break;
        }
        previous_node = node;
        node = node->next;
    } while (node != start);

    return start;
}

void delete_list(Node *start) {
    Node *node = start;
    Node *next_node;

    while (node != nullptr) {
        next_node = node->next;
        delete node;
        node = next_node;
    }
}

Node *reverse_list(Node *start) {
    if (start->next == start) {
        return start;
    }

    if (start->next->next == start) {
        return start->next;
    }

    Node *last_node = find_last_node(start);

    Node *node = start;
    Node *next = start->next;
    Node *further = next->next;

    start->next = last_node;
    while (node != last_node) {
        next->next = node;
        node = next;
        next = further;
        further = further->next;
    }

    return last_node;
}

void insert_node(Node *start, int data, int target) {
    Node *target_node = start;
    while (target_node->data != target && target_node->next != start) {
        target_node = target_node->next;
    }

    if (target_node->data != target) {
        return;
    }

    Node *new_node = make_node(target_node->next, data);
    target_node->next = new_node;
}

void merge_lists(Node *start1, Node *start2) {
    Node *last_node1 = find_last_node(start1);
    Node *last_node2 = find_last_node(start2);

    last_node1->next = start2;
    last_node2->next = start1;
}

Node *move_start(Node *start, int target) {
    Node *node = start;
    while (node->data != target && node->next != start) {
        node = node->next;
    }

    if (node->data == target) {
        return node;
    }

    return start;
}
